from pyzabbix.api import ZabbixAPI
from pynotifier import Notification
from time import sleep
import urllib.request, yaml


with open("config.yaml") as configFile:
    config = yaml.load(configFile, Loader=yaml.SafeLoader)

try:
    urllib.request.urlopen(config['connection']['server'], timeout=1)
except urllib.request.URLError:
    print("Cannot connect to zabbix server, check IP address or try again later.")
    exit(0)

zapi = ZabbixAPI(config['connection']['server'], user=config['connection']['login'], password=config['connection']['password'])
lastNotificationID = 0 

try:
    while True:
        requestResult = zapi.do_request('problem.get', {'sortfield':'eventid', 'sortorder':'DESC'})

        if requestResult['result'][0]['eventid'] != lastNotificationID:
            Notification(
                    title='Zabbix Problem Occured!',
                    description=requestResult['result'][0]['name'],
                    icon_path='path/to/image/file/icon.png', 
                    duration=1,                             
                    urgency=Notification.URGENCY_CRITICAL
            ).send()
            lastNotificationID = requestResult['result'][0]['eventid']

        sleep(5)
except KeyboardInterrupt:
    zapi.user.logout()
    exit(0)

zapi.user.logout()
